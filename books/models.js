class Book {
    constructor (entry) {
        this.id = entry.id;
        this.nume = entry.nume;
        this.genre = entry.genre;
        this.author = {
            id: entry.id_autor,
            nume: entry.nume_autor
        }
    }
}

module.exports = {
    Book
}