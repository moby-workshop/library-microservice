const Router = require('express').Router();

const authorsControllers = require('./authors/controllers.js');
const booksControllers = require('./books/controllers.js');

Router.use('/authors', authorsControllers);
Router.use('/books', booksControllers);

module.exports = Router;